package model

import (
	"encoding/json"
	"time"

	"github.com/google/uuid"
)

type Event struct {
	ID           uuid.UUID       `json:"id"`
	RegisteredAt time.Time       `json:"registeredAt"`
	OccurredAt   time.Time       `json:"occurredAt"`
	SubjectIDs   []uuid.UUID     `json:"subjectIds"`
	EventType    string          `json:"eventType"`
	EventData    json.RawMessage `json:"eventData"`
}

// type Municipality struct {
// 	Name string `json:"name"`
// 	Code string `json:"code"`
// }

// BirthEventData is used to decode events from the seeder and simulator
type BirthEventData struct {
	Birth struct {
		DateOfBirth  string `json:"dateOfBirth"`
		PlaceOfBirth struct {
			Code string `json:"code"`
			Name string `json:"name"`
		} `json:"placeOfBirth"`
		CountryOfBirth struct {
			Code string `json:"code"`
			Name string `json:"name"`
		} `json:"countryOfBirth"`
	} `json:"birth"`
	BSN    int    `json:"bsn"`
	Gender string `json:"gender"`
	Naming struct {
		FirstName  string `json:"firstName"`
		Prefix     string `json:"prefix"`
		FamilyName string `json:"familyName"`
	} `json:"naming"`
	AddressRegistration struct {
		AddressID string `json:"address"`
	} `json:"addressRegistration"`
}

// type NamingEventData struct {
// 	GivenName    string       `json:"givenName"`
// 	Surname      string       `json:"surName"`
// 	Municipality Municipality `json:"municipality"`
// }

// BirthEvent is used for sending requests to the API
type BirthEvent struct {
	ID     uuid.UUID `json:"id"`
	BSN    string    `json:"bsn"`
	BornAt time.Time `json:"bornAt"`
	Sex    string    `json:"sex"`
}

// type NamingEvent struct {
// 	Name string `json:"name"`
// }

type DeathEvent struct {
	DiedAt time.Time `json:"diedAt"`
}

// type RegisteredOnAddressEventData struct {
// 	AddressID    uuid.UUID    `json:"address"`
// 	Municipality Municipality `json:"municipality"`
// }

type AddressChangedEvent struct {
	RegisterAddressID uuid.UUID `json:"registeredAddressId"`
}
