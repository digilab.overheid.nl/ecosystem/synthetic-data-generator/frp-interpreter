module gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/frp-interpreter

go 1.22.0

toolchain go1.22.1

replace gitlab.com/digilab.overheid.nl/ecosystem/frrd/claim-api => ../frrd/claim-api

require (
	github.com/google/uuid v1.6.0
	github.com/nats-io/nats.go v1.28.0
	github.com/spf13/cobra v1.7.0
	gitlab.com/digilab.overheid.nl/ecosystem/frrd/claim-api v0.0.0-20240417090938-0473a0cb3890
)

require (
	github.com/apapsch/go-jsonmerge/v2 v2.0.0 // indirect
	github.com/getkin/kin-openapi v0.123.0 // indirect
	github.com/go-chi/chi/v5 v5.0.12 // indirect
	github.com/go-openapi/jsonpointer v0.21.0 // indirect
	github.com/go-openapi/swag v0.23.0 // indirect
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/invopop/yaml v0.2.0 // indirect
	github.com/josharian/intern v1.0.0 // indirect
	github.com/klauspost/compress v1.16.7 // indirect
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/mohae/deepcopy v0.0.0-20170929034955-c48cc78d4826 // indirect
	github.com/nats-io/nats-server/v2 v2.9.21 // indirect
	github.com/nats-io/nkeys v0.4.4 // indirect
	github.com/nats-io/nuid v1.0.1 // indirect
	github.com/oapi-codegen/runtime v1.1.1 // indirect
	github.com/perimeterx/marshmallow v1.1.5 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	gitlab.com/digilab.overheid.nl/ecosystem/fsc-vwl-api v0.0.0-20240313144656-6e78c7411048 // indirect
	golang.org/x/crypto v0.17.0 // indirect
	golang.org/x/sys v0.15.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
