package application

import (
	"context"
	"encoding/json"
	"fmt"

	"github.com/google/uuid"
	frrd "gitlab.com/digilab.overheid.nl/ecosystem/frrd/claim-api"
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/frp-interpreter/model"
)

func (app *Application) HandleBirthEvent(ctx context.Context, event *model.Event) error {
	data := new(model.BirthEventData)
	if err := json.Unmarshal(event.EventData, data); err != nil {
		return fmt.Errorf("json unmarshal failed: %w", err)
	}

	// Create several claims based on the birth registration event
	claimantID := uuid.UUID{} // TODO: claimant ID of the interpreter

	if len(event.SubjectIDs) == 0 {
		return fmt.Errorf("no subject ID in event")
	}

	subjectID := event.SubjectIDs[0]

	// Create the Burgerservicenummer claim
	app.client.CreateClaimWithResponse(ctx, nil, frrd.CreateClaimJSONRequestBody{
		Input: frrd.Claim{
			Type:     frrd.Burgerservicenummer,
			Kind:     frrd.Assertion,
			Claimant: claimantID,
			Subject:  subjectID,
			Data: frrd.ClaimData{
				"value": data.BSN, // IMPROVE: convert to string?
			},
			RegistrationStart: event.RegisteredAt, // IMPROVE: use time.Now()? Also below
			ValidStart:        &event.OccurredAt,
		},
	})

	// Create the Voornamen claim
	app.client.CreateClaimWithResponse(ctx, nil, frrd.CreateClaimJSONRequestBody{
		Input: frrd.Claim{
			Type:     frrd.Voornamen,
			Kind:     frrd.Assertion,
			Claimant: claimantID,
			Subject:  subjectID,
			Data: frrd.ClaimData{
				"value": data.Naming.FirstName,
			},
			RegistrationStart: event.RegisteredAt,
			ValidStart:        &event.OccurredAt,
		},
	})

	// Create the Voorvoegsel claim
	app.client.CreateClaimWithResponse(ctx, nil, frrd.CreateClaimJSONRequestBody{
		Input: frrd.Claim{
			Type:     frrd.Voorvoegsel,
			Kind:     frrd.Assertion,
			Claimant: claimantID,
			Subject:  subjectID,
			Data: frrd.ClaimData{
				"value": data.Naming.Prefix,
			},
			RegistrationStart: event.RegisteredAt,
			ValidStart:        &event.OccurredAt,
		},
	})

	// Create the Geslachtsnaam claim
	app.client.CreateClaimWithResponse(ctx, nil, frrd.CreateClaimJSONRequestBody{
		Input: frrd.Claim{
			Type:     frrd.Geslachtsnaam,
			Kind:     frrd.Assertion,
			Claimant: claimantID,
			Subject:  subjectID,
			Data: frrd.ClaimData{
				"value": data.Naming.FamilyName,
			},
			RegistrationStart: event.RegisteredAt,
			ValidStart:        &event.OccurredAt,
		},
	})

	// Create the Geboortedatum claim
	app.client.CreateClaimWithResponse(ctx, nil, frrd.CreateClaimJSONRequestBody{
		Input: frrd.Claim{
			Type:     frrd.Geboortedatum,
			Kind:     frrd.Assertion,
			Claimant: claimantID,
			Subject:  subjectID,
			Data: frrd.ClaimData{
				"value": data.Birth.DateOfBirth,
			},
			RegistrationStart: event.RegisteredAt,
			ValidStart:        &event.OccurredAt,
		},
	})

	fmt.Println("crated claims for GeboorteVastgesteld event")

	return nil
}

// func (app *Application) HandleNamingEvent(ctx context.Context, event *model.Event) error {
// 	data := new(model.NamingEventData)
// 	if err := json.Unmarshal(event.EventData, data); err != nil {
// 		return fmt.Errorf("json unmarshal failed: %w", err)
// 	}

// 	if app.Filter(data.Municipality.Code) {
// 		return nil
// 	}

// 	namingEvent := model.NamingEvent{
// 		Name: fmt.Sprintf("%s %s", data.GivenName, data.Surname),
// 	}

// 	if err := app.Request(ctx, http.MethodPatch, fmt.Sprintf("/people/%s", event.SubjectIDs[0].String()), namingEvent, nil); err != nil {
// 		return fmt.Errorf("request failed: %w", err)
// 	}

// 	return nil
// }

func (app *Application) HandleDeathEvent(ctx context.Context, event *model.Event) error {
	// deathEvent := model.DeathEvent{
	// 	DiedAt: event.OccurredAt,
	// }

	// if err := app.Request(ctx, http.MethodPatch, fmt.Sprintf("/people/%s", event.SubjectIDs[0]), deathEvent, nil); err != nil {
	// 	return fmt.Errorf("request failed: %w", err)
	// }

	return nil
}

// func (app *Application) HandleRegisteredOnAddressEvent(ctx context.Context, event *model.Event) error {
// 	data := new(model.RegisteredOnAddressEventData)
// 	if err := json.Unmarshal(event.EventData, data); err != nil {
// 		return fmt.Errorf("json unmarshal failed: %w", err)
// 	}

// 	if app.Filter(data.Municipality.Code) {
// 		return nil
// 	}

// 	addressRegisteredEvent := model.AddressChangedEvent{
// 		RegisterAddressID: data.AddressID,
// 	}

// 	if err := app.Request(ctx, http.MethodPatch, fmt.Sprintf("/people/%s", event.SubjectIDs[0]), addressRegisteredEvent, nil); err != nil {
// 		return fmt.Errorf("request failed: %w", err)
// 	}

// 	return nil
// }

func (app *Application) HandleAddressWrittenOutEvent(ctx context.Context, event *model.Event) error {
	// addressWrittenOutEvent := model.AddressChangedEvent{
	// 	RegisterAddressID: uuid.UUID{},
	// }

	// if err := app.Request(ctx, http.MethodPatch, fmt.Sprintf("/people/%s", event.SubjectIDs[0]), addressWrittenOutEvent, nil); err != nil {
	// 	return fmt.Errorf("request failed: %w", err)
	// }

	return nil
}
