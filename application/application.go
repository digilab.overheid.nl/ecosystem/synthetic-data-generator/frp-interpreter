package application

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"log"

	"github.com/nats-io/nats.go"
	frrd "gitlab.com/digilab.overheid.nl/ecosystem/frrd/claim-api"
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/frp-interpreter/model"
)

type Application struct {
	client  *frrd.ClientWithResponses
	baseURL string
}

var ErrResponseFailed = errors.New("response failed with")

func NewApplication(baseURL string) *Application {
	client, err := frrd.NewClientWithResponses(baseURL)
	if err != nil {
		log.Fatal("error creating application", err)
	}

	return &Application{
		client:  client,
		baseURL: baseURL,
	}
}

func (app *Application) HandleEvents(msg *nats.Msg) {
	event := new(model.Event)
	if err := json.Unmarshal(msg.Data, event); err != nil {
		fmt.Print(fmt.Errorf("unmarshal failed: %w", err))
	}

	switch event.EventType {
	case "GeboorteVastgesteld":
		if err := app.HandleBirthEvent(context.Background(), event); err != nil {
			fmt.Println(fmt.Errorf("handle birth event failed: %w", err))
		}
	// case "NamenVastgesteld":
	// 	if err := app.HandleNamingEvent(context.Background(), event); err != nil {
	// 		fmt.Println(fmt.Errorf("handle naming event failed: %w", err))
	// 	}
	case "OverlijdenVastgesteld":
		if err := app.HandleDeathEvent(context.Background(), event); err != nil {
			fmt.Println(fmt.Errorf("handle death event failed: %w", err))
		}
	// case "AdresIngeschreven":
	// 	if err := app.HandleRegisteredOnAddressEvent(context.Background(), event); err != nil {
	// 		fmt.Println(fmt.Errorf("handle address registered event failed: %w", err))
	// 	}
	case "AdresUitgeschreven":
		if err := app.HandleAddressWrittenOutEvent(context.Background(), event); err != nil {
			fmt.Println(fmt.Errorf("handle address written out event failed: %w", err))
		}
	default:
		fmt.Printf("Event type: %s unknown\n", event.EventType)
	}
}
